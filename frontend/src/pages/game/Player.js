import React from 'react';

function Player(props) {

    return (
        <li>
            Player {props.id}: {props.player}
        </li>
    )
}

export default Player;